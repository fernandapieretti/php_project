var gulp = require('gulp');
var minifyCSS = require('gulp-minify-css');
var minifyJS = require('gulp-jsmin');
var concat = require('gulp-concat');

gulp.task('minifyCSS'  , function() {
    gulp.src('_estilos/*.css')
    .pipe(minifyCSS())
    .pipe(concat('styles.min.css'))
    .pipe(gulp.dest('_estilos'));
});

gulp.task('minifyJS', function() {
    gulp.src('_js/*.js')
    .pipe(minifyJS())
    .pipe(concat('scripts.min.js'))
    .pipe(gulp.dest('_js'));
});

gulp.task('minifyCSSPriv', function() {
    gulp.src(['css/*/*.css', 'css/*.css' ])
    .pipe(minifyCSS())
    .pipe(concat('styles.min.css'))
    .pipe(gulp.dest('css'));
});

gulp.task('minifyJSPriv', function() {
    gulp.src(['js/*/*.js', 'js/*.js'])
    .pipe(minifyJS())
    .pipe(concat('scripts.min.js'))
    .pipe(gulp.dest('js'));
});

gulp.task('default', ['minifyCSS', 'minifyJS', 'minifyCSSPriv', 'minifyJSPriv']);