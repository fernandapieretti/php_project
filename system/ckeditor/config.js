﻿/**
 * @license Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	config.filebrowserBrowseUrl = 'http://www.catscanada.esy.es/filemanager/index.php';
	config.filebrowserImageBrowseUrl = 'http://www.catscanada.esy.es/filemanager/index.php';
	config.removePlugins = 'elementspath';
	config.allowedContent = true;
	CKEDITOR.dtd.$removeEmpty.span = 0;
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
};


/*
CKEDITOR.editorConfig = function( config )
{
   
   config.removePlugins =  'elementspath,enterkey,entities,forms,pastefromword,htmldataprocessor,specialchar,horizontalrule,wsc' ;
   
	   
   CKEDITOR.config.toolbar = [
   ['Styles','Format','Font','FontSize'],
   '/',
   ['Bold','Italic','Underline','StrikeThrough','-','Undo','Redo','-','Cut','Copy','Paste','Replace','-','Outdent','Indent','-'],
   '/',
   ['NumberedList','BulletedList','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
   ['Table','-','TextColor','BGColor','Source']
] ;

};*/