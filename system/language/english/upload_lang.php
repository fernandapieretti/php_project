<?php

$lang['upload_userfile_not_set'] = "Arquivo não selecionado.";
$lang['upload_file_exceeds_limit'] = "O arquivo enviado excede o tamanho máximo permitido.";
$lang['upload_file_exceeds_form_limit'] = "O arquivo enviado excede o tamanho máximo permitido.";
$lang['upload_file_partial'] = "Erro ao upar arquivo.";
$lang['upload_no_temp_directory'] = "A pasta temporária está faltando.";
$lang['upload_unable_to_write_file'] = "O arquivo não pôde ser gravado em disco.";
$lang['upload_stopped_by_extension'] = "O upload do arquivo foi interrompido.";
$lang['upload_no_file_selected'] = "Você não selecionou um arquivo para upload.";
$lang['upload_invalid_filetype'] = "O tipo de arquivo que você está tentando fazer upload não é permitido.";
$lang['upload_invalid_filesize'] = "O arquivo que você está tentando fazer o upload é maior do que o tamanho permitido.";
$lang['upload_invalid_dimensions'] = "A imagem que você está tentando fazer upload de exceedes a altura ou a largura máxima .";
$lang['upload_destination_error'] = "Foi encontrado um problema ao tentar mover o arquivo enviado para o destino final.";
$lang['upload_no_filepath'] = "A pasta de destino de upload parece não existir.";
$lang['upload_no_file_types'] = "Você não especificou quaisquer tipos de arquivos permitidos.";
$lang['upload_bad_filename'] = "O nome do arquivo que você está tentando upar já existe no servidor.";
$lang['upload_not_writable'] = "A pasta de destino de upload não parece ser gravável.";


/* End of file upload_lang.php */
/* Location: ./system/language/english/upload_lang.php */