<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$route['default_controller'] = "principal";
$route['/'] = "principal";
$route['login'] = 'login/login';
$route['404_override'] = 'principal/erro404';

$route['post/(:any)/(:any)/(:num)'] = 'principal/post/$3';
$route['categoria/(:any)/(:num)'] = 'principal/categoria/$2';
$route['texto/(:any)/(:num)'] = 'principal/texto/$2';
$route['ler-em-(:any)'] = 'principal/lingua/$1';
$route['sitemap'] = 'principal/sitemap';

?>
