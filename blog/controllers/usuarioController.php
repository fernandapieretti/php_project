<?php

class UsuarioController extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

	function editUsuario() {
		$this->load->model("Usuario_model", "usuario");
		$id = $this->input->post("id");
		
		$update = array(
			"nome" => $this->input->post("nome"),
			"login" => $this->input->post("login"),
			"senha" => $this->input->post("senha")
		);
		
		$this->load->model('Empresa_model', 'empresa');
		$servidor["servidor"] = $this->empresa->getAll();	

		if ($this->usuario->update($update,$id) > 0) {
			$data["sucesso"] = "Salvo com sucesso.";
			
			$mensagem = "&nbsp;";
			$mensagem = $mensagem . "<img src='" . base_url() . "_imagens/logo.png' alt='" . $servidor["servidor"][0]->razaoSocial ."' title='" . $servidor["servidor"][0]->empresa ."' /><br /><br />";
			$mensagem = $mensagem . "<h1>Alteração dos dados de acesso</h1><br /><br />";
			$mensagem = $mensagem . "<p>Os dados do administrador foram alterados no site <b>" . $servidor["servidor"][0]->razaoSocial ."</b></p>";
			$mensagem = $mensagem . "<p>Confira os novos dados de acesso ao sistema gerenciador do site:</p>";
			$mensagem = $mensagem . "<p><b>Nome</b>: " . $this->input->post("nome") . "<br />";
			$mensagem = $mensagem . "<b>Login</b>: " . $this->input->post("login") . "<br />";
			$mensagem = $mensagem . "<b>Senha</b>: " .  $this->input->post("senha") . "</p><br /><br />";
			$mensagem = $mensagem . "<p>Guarde este e-mail para o caso de esquecer sua senha.</p><br /><br />";
			$mensagem = $mensagem . "<p>Atenciosamente,</p>";
			$mensagem = $mensagem . "<p><b>" . $servidor["servidor"][0]->razaoSocial ."</b></p>";
			
			$this->load->library('email');
			
			$config['smtp_host'] = $servidor["servidor"][0]->smtp;
			$config['smtp_user'] = $servidor["servidor"][0]->usuario;
			$config['smtp_pass'] = $servidor["servidor"][0]->senha;
			$config['smtp_port'] = $servidor["servidor"][0]->porta;
			$this->email->initialize($config);
			
			$this->email->from($servidor["servidor"][0]->usuario, $servidor["servidor"][0]->razaoSocial);
			$this->email->to($servidor["servidor"][0]->email);
			$this->email->subject("Senha alterada | " . $servidor["servidor"][0]->razaoSocial);
			$this->email->message($mensagem);
	
			$message = array();
			$this->email->send();
		} else {
			$data["erro"] = "Erro ao salvar.";
		}
		
		$data["usuario"] = $this->usuario->getAll();
		$this->load->vars($data);
		$this->load->view("priv/usuario/editUsuario");
    }
    
    function editarUsuarioAction($id, $mensagem = array()) {   
        $this->load->model('Usuario_model', 'usuario');
        $data["usuario"] = $this->usuario->buscarPorId($id);
		
        $data["sucesso"] = $mensagem["sucesso"];
        $data["error"] = $mensagem["error"];
			   
        $this->load->vars($data);
        $this->load->view("priv/usuario/editUsuario");
    } 
}

?>