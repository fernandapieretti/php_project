<?php

class PostController extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    function index() {
		$this->load->model('Post_model', 'post');
		$data["post"] = $this->post->getAll();
		
		$this->load->vars($data);
		$this->load->view("priv/post/list");
    }
    
	function addAction() {
		$this->load->model('Categoria_model', 'categoria');
		$data["categorias"] = $this->categoria->getAll();

		$this->load->vars($data);
		$this->load->view("priv/post/add");
	}
	
	function add() {
		$this->load->model("Post_model", "post");		
		$insert = array(
			"data" => implode("-",array_reverse(explode("/",$this->input->post("data")))),
			"visualizacao" => 0,
			"titulo" => $this->input->post("titulo"),
            "title" => $this->input->post("title"),
			"palavras" => $this->input->post("palavras"),
			"descricao" => $this->input->post("descricao"),
            "description" => $this->input->post("description"),
			"resumo" => $this->input->post("resumo"),
            "summary" => $this->input->post("summary"),
			"texto" => $this->input->post("texto"),
            "text" => $this->input->post("text"),
			"autor" => $this->input->post("autor"),
			"idcategoria" => $this->input->post("idcategoria"),
			"status" => $this->input->post("status")
		);		
		$id  = $this->post->add_record($insert);	
			
		if ($id > 0) {
			$data["sucesso"] = "Salvo com sucesso.";
		} else {
			$data["error"] = "Erro ao salvar.";
		}
		$this->editAction($id, $data);
	}
	
	function delete($id) {
		$this->load->model("Post_model", "post");		
		
		if ($this->post->delete($id) > 0) {
			$data["sucesso"] = "Excluído com sucesso.";
		} else {
			$data["error"] = "Erro ao excluir.";
		}
		
		$data["post"] = $this->post->getAll();
		$this->load->vars($data);
		$this->load->view("priv/post/list");
	}
	
	function edit() {
		$this->load->model("Post_model", "post");
		$id     = $this->input->post("id");
		$update = array(
			"data" => implode("-",array_reverse(explode("/",$this->input->post("data")))),
			"visualizacao" => $this->input->post("visualizacao"),
			"titulo" => $this->input->post("titulo"),
            "title" => $this->input->post("title"),
			"palavras" => $this->input->post("palavras"),
			"descricao" => $this->input->post("descricao"),
            "description" => $this->input->post("description"),
			"resumo" => $this->input->post("resumo"),
            "summary" => $this->input->post("summary"),
			"texto" => $this->input->post("texto"),
            "text" => $this->input->post("text"),
			"autor" => $this->input->post("autor"),
			"idcategoria" => $this->input->post("idcategoria"),
			"status" => $this->input->post("status")
		);
		
		if ($this->post->update($id, $update) > 0) {
			$data["sucesso"] = "Salvo com sucesso.";			
		} else {
			$data["erro"] = "Erro ao salvar.";
		}
		
		$this->editAction($id, $data);
	}

	function editAction($id, $mensagem = array()) {
		$data["sucesso"] = $mensagem["sucesso"];
		$data["error"]   = $mensagem["error"];
		
		$this->load->model('Post_model', 'post');
		$data["post"] = $this->post->buscarPorId($id);
		
		$this->load->model('Categoria_model', 'categoria');
		$data["categorias"] = $this->categoria->getAll();
		
		$this->load->vars($data);
		$this->load->view("priv/post/edit");
	}
	
	function uploadImagem($id) {
		$this->load->model('Post_model', 'post');
		$delete["delete"] = $this->post->buscarPorId($id);
		foreach ($delete as $row) { 
			if ($row->imagem) { unlink("./upload/post/" . $row->imagem); }
		}
		
		$config["upload_path"]   = "./upload/post/";
		$config["allowed_types"] = "gif|jpg|png";
		$config["file_name"]     = "post_" . $id . "_" . rand(00, 9999);
		$config["overwrite"]     = TRUE;
		$config["remove_spaces"] = TRUE;		
		$this->load->library("upload", $config);
		
		if ($this->upload->do_upload("userfile")) {
			$update = array(
				"imagem" => $this->upload->file_name
			);
			if ($this->post->update($id, $update) > 0) {
				$data["sucesso"] = "Imagem atualizada com sucesso.";
			} else {
				$data["error"] = "Erro ao atualizada imagem no banco de dados.";
			}
		} else {
			$data["error"] = "Erro ao fazer upload da imagem: " . $this->upload->display_errors();
		}
		$this->editAction($id, $data);
	}
	
}
?>