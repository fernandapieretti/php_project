<?php

class EmpresaController extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    function index() {
        $this->load->model('Empresa_model', 'empresa');
        $data["empresa"] = $this->empresa->getAll();

        $this->load->vars($data);
        $this->load->view("priv/empresa/editEmpresa");
    }
    
    function editarEmpresaAction($id) {
        $this->load->model("Empresa_model", "empresa");
        $data["empresa"] = $this->empresa->buscarPorId($id);
		
		$this->load->vars($data);
		$this->load->view("priv/empresa/editEmpresa");
    }

    function editEmpresa() {
        $this->load->model("Empresa_model", "empresa");
        $id = $this->input->post("id");
        
        $update = array(
			"titulo" => $this->input->post("titulo"),
			"telefone" => $this->input->post("telefone"),
			"endereco" => $this->input->post("endereco"),
			"email" => $this->input->post("email"),
			"facebook" => $this->input->post("facebook"),
			"twitter" => $this->input->post("twitter"),
			"plus" => $this->input->post("plus"),
			"instagram" => $this->input->post("instagram"),
			"youtube" => $this->input->post("youtube"),
			"ga" => $this->input->post("ga"),
			"smtp" => $this->input->post("smtp"),
			"porta" => $this->input->post("porta"),
			"usuario" => $this->input->post("usuario"),
			"senha" => $this->input->post("senha")
        );

        if ($this->empresa->update($id, $update) > 0) {
			$data["sucesso"] = "Salvo com sucesso.";
			$data["empresa"] = $this->empresa->getAll();
			
			$this->load->vars($data);
			$this->load->view("priv/empresa/editEmpresa");
        }
    }
}

?>