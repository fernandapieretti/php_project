<?php

class CategoriaController extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    function index() {
		$this->load->model('Categoria_model', 'categoria');
		$data["categorias"] = $this->categoria->getAll();
		
		$this->load->vars($data);
		$this->load->view("priv/categoria/list");
    }
    
    function addAction() {
		$this->load->view("priv/categoria/add");
    }
	
    function add() {
		$this->load->model('Categoria_model', 'categoria');

		$insert = array(
            "titulo" => $this->input->post("titulo"),
            "title" => $this->input->post("title")
        );
		$id = $this->categoria->add_record($insert);
		
        if ($id > 0) {
			$data["sucesso"] = "Salvo com sucesso.";
			$data["categorias"] = $this->categoria->getAll();
			$this->load->vars($data);
			$this->load->view("priv/categoria/list");
        }
    }
	
	function editAction($id, $mensagem) {
		$this->load->model('Categoria_model', 'categoria');
		$data["categoria"] = $this->categoria->buscarPorId($id);
	
		$data["erro"] = $mensagem["erro"];
		$data["sucesso"] = $mensagem["sucesso"];
		
		$this->load->vars($data);
		$this->load->view("priv/categoria/edit");
	}

    function edit() {
        $this->load->model("Categoria_model", "categoria");
        $id = $this->input->post("id");
		
        $update = array(
            "titulo" => $this->input->post("titulo"),
            "title" => $this->input->post("title")
        );
		$result = $this->categoria->update($id, $update);

        if ($result > 0) {
			$data["sucesso"] = "Salvo com sucesso.";
			$data["categorias"] = $this->categoria->getAll();
			$this->load->vars($data);
			$this->load->view("priv/categoria/list");
        }
    }
	
    function delete($id) {
		$this->load->model("Categoria_model", "categoria");
		$result = $this->categoria->delete($id);
		
		if ($result > 0) {	
			$data["sucesso"] = "Excluído com sucesso.";
			$data["categorias"] = $this->categoria->getAll();
			
			$this->load->vars($data);
			$this->load->view("priv/categoria/list");
		}
    }
}
?>