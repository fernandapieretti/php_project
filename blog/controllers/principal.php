<?php
session_start();

class Principal extends CI_Controller {
	
	function __construct() {
		parent::__construct();
	}
		
	public function url($str) {
		$str = strtolower(utf8_decode($str)); $i=1;
		$str = strtr($str, utf8_decode('àáâãäåæçèéêëìíîïñòóôõöøùúûýýÿ'), 'aaaaaaaceeeeiiiinoooooouuuyyy');
		$str = preg_replace("/([^a-z0-9])/",'-',utf8_encode($str));
		while($i>0) $str = str_replace('--','-',$str,$i);
		if (substr($str, -1) == '-') $str = substr($str, 0, -1);
		return $str;
	}
	
	public function erro404() {
		$this->load->model('Empresa_model', 'empresa');
		$data["empresa"] = $this->empresa->getAll();
		
		$this->load->model('Categoria_model', 'categoria');
		$data["categorias"] = $this->categoria->getAll();
		
		$this->load->model('Texto_model', 'texto');
		$data["sobre"] = $this->texto->buscarPorId(1);
		
		$data["title"] = "Erro 404 - " . $data["empresa"][0]->titulo;
		$data["description"] = "Esta página não foi encontrada.";
		$data["image"] = base_url() . "_imagens/logo.png";
		$data["dataLayerCategoria"] = "Texto";
		$data["dataLayerArea"] = "Erro 404";
		$data["dataLayerPost"] = "";
		
		$this->load->vars($data);
		$this->load->view('_paginas/erro404');
	}
	
	public function arearestrita() {		
		$this->load->model('Post_model', 'post');
		$data["maislidos"] = $this->post->buscarMaisLidos();
		
		$this->load->vars($data);
		$this->load->view('priv/default');
	}
	
	public function lingua($lingua) {
		$_SESSION['lingua'] = $lingua;
		redirect($_SERVER['HTTP_REFERER'], 'refresh');
	}
	
	public function sitemap() {
		$this->load->model('Categoria_model', 'categoria');
		$data["categorias"] = $this->categoria->getAll();
		
		$this->load->model('Post_model', 'post');
		$data["posts"] = $this->post->buscarAtivos();
		
		$this->load->vars($data);
		$this->load->view('_paginas/sitemap');
	}
	
	public function index() {
		$this->load->model('Empresa_model', 'empresa');
		$data["empresa"] = $this->empresa->getAll();
		
		$this->load->model('Categoria_model', 'categoria');
		$data["categorias"] = $this->categoria->getAll();
		
		$this->load->model('Texto_model', 'texto');
		$data["sobre"] = $this->texto->buscarPorId(1);
		
		$this->load->model('Post_model', 'post');
		$data["posts"] = $this->post->buscarAtivos();
		$data["maislidos"] = $this->post->buscarMaisLidos();
		
		$data["title"] = $data["empresa"][0]->titulo;
		$data["description"] =  $data["empresa"][0]->descricao;
		$data["keywords"] =  $data["empresa"][0]->palavras;
		$data["image"] = base_url() . "_imagens/logo.png";
		$data["dataLayerCategoria"] = "Home";
		$data["dataLayerArea"] = "";
		$data["dataLayerPost"] = "";
		$data["class"] = "home";
		
		$this->load->vars($data);
		$this->load->view('_paginas/index');
	}
	
	public function categoria($id) {
		$this->load->model('Empresa_model', 'empresa');
		$data["empresa"] = $this->empresa->getAll();
		
		$this->load->model('Texto_model', 'texto');
		$data["sobre"] = $this->texto->buscarPorId(1);
		
		$this->load->model('Categoria_model', 'categoria');
		$data["categorias"] = $this->categoria->getAll();
		$data["categoria"] = $this->categoria->buscarPorId($id);
		
		$this->load->model('Post_model', 'post');
		$data["posts"] = $this->post->buscarAtivosPorCategoria($id);
		$data["maislidos"] = $this->post->buscarMaisLidos();
		
		$data["title"] = $data["categoria"][0]->titulo . " - " . $data["empresa"][0]->titulo;
		$data["description"] =  $data["categoria"][0]->titulo;
		$data["keywords"] =  $data["categoria"][0]->palavras;
		$data["image"] = base_url() . "_imagens/logo.png";
		$data["dataLayerCategoria"] = "Categoria";
		$data["dataLayerArea"] = $data["categoria"][0]->titulo;
		$data["dataLayerPost"] = "";
		
		$this->load->vars($data);
		$this->load->view('_paginas/categorias');
	}
	
	public function texto($id) {
		$this->load->model('Empresa_model', 'empresa');
		$data["empresa"] = $this->empresa->getAll();
		
		$this->load->model('Categoria_model', 'categoria');
		$data["categorias"] = $this->categoria->getAll();
		
		$this->load->model('Texto_model', 'texto');
		$data["texto"] = $this->texto->buscarPorId($id);
		$data["sobre"] = $this->texto->buscarPorId(1);
		
		$this->load->model('Post_model', 'post');
		$data["maislidos"] = $this->post->buscarMaisLidos();
		
		$data["title"] = $data["texto"][0]->titulo . " - " . $data["empresa"][0]->titulo;
		$data["description"] =  $data["texto"][0]->descricao;
		$data["keywords"] =  $data["categoria"][0]->palavras;
		$data["image"] = base_url() . "upload/texto/" . $data["texto"][0]->imagem;
		$data["dataLayerCategoria"] = "Texto";
		$data["dataLayerArea"] = $data["texto"][0]->titulo;
		$data["dataLayerPost"] = "";
			
		$this->load->vars($data);
		$this->load->view('_paginas/texto');
	}
	
	public function post($id) {
		$this->load->model('Empresa_model', 'empresa');
		$data["empresa"] = $this->empresa->getAll();
		
		$this->load->model('Categoria_model', 'categoria');
		$data["categorias"] = $this->categoria->getAll();
		
		$this->load->model('Post_model', 'post');
		$this->post->visualizacao($id);
		$data["post"] = $this->post->buscarPorIdComCategoria($id);
		$data["anterior"] = $this->post->buscarAnterior($data["post"][0]->data);
		$data["proximo"] = $this->post->buscarProximo($data["post"][0]->data);
		$data["maislidos"] = $this->post->buscarMaisLidos();
		$idautor = $data["post"][0]->autor + 1;
		
		$this->load->model('Texto_model', 'texto');
		$data["sobre"] = $this->texto->buscarPorId(1);
		$data["autor"] = $this->texto->buscarPorId($idautor);
		
		$data["title"] = $data["post"][0]->titulo . " - " . $data["post"][0]->categoria . " - " . $data["empresa"][0]->titulo;
		$data["description"] =  $data["post"][0]->resumo;
		$data["image"] = base_url() . "upload/post/" . $data["post"][0]->imagem;
		$data["keywords"] =  $data["post"][0]->palavras;
		$data["dataLayerCategoria"] = "Post";
		$data["dataLayerArea"] = $data["post"][0]->categoria;
		$data["dataLayerPost"] = $data["post"][0]->titulo;
		
		$this->load->vars($data);
		$this->load->view('_paginas/post');
	}
	
	public function deletarVisualizacoes() {
		$this->load->model('Post_model', 'post');
		$this->post->deletarVisualizacoes();
	}
	
	public function contato() {
		$this->load->model('Empresa_model', 'empresa');
		$data["empresa"] = $this->empresa->getAll();
		
		$this->load->model('Categoria_model', 'categoria');
		$data["categorias"] = $this->categoria->getAll();
		
		$this->load->model('Texto_model', 'texto');
		$data["sobre"] = $this->texto->buscarPorId(1);
		
		$this->load->model('Post_model', 'post');
		$data["maislidos"] = $this->post->buscarMaisLidos();
		
		$data["title"] = "Contato - " . $data["empresa"][0]->titulo;
		$data["description"] =  "Entre em contato com a gente =)";
		$data["image"] = base_url() . "_imagens/logo.png";
		
		$this->load->vars($data);
		$this->load->view('_paginas/contato');
	}
	
	public function enviarcontato() {
		$this->load->model('Empresa_model', 'empresa');
		$data["empresa"] = $this->empresa->getAll();
		
		$mensagem = "&nbsp;";
		$mensagem = $mensagem . "<img src='" . base_url() . "_imagens/logo.png' alt='" . $data["empresa"][0]->titulo . "' title='" . $data["empresa"][0]->titulo . "' /><br /><br />";
		$mensagem = $mensagem . "<h2>Contato recebido pelo site.</h2>";
		$mensagem = $mensagem . "<p>Nome: " . $this->input->post("nome") . "</p>";
		$mensagem = $mensagem . "<p>E-mail: " . $this->input->post("email") . "</p>";
		$mensagem = $mensagem . "<p>Mensagem: " . $this->input->post("mensagem") . "</p>";
		$mensagem = $mensagem . "<br><br><br>";
		$mensagem = $mensagem . "<p>Atenciosamente,<br>";
		$mensagem = $mensagem . "<b>Blog</b></p><br />";
		
		$this->load->library('email');					
		$config['smtp_host'] = $data["empresa"][0]->smtp;
		$config['smtp_user'] = $data["empresa"][0]->usuario;
		$config['smtp_pass'] = $data["empresa"][0]->senha;
		$config['smtp_port'] = $data["empresa"][0]->porta;
		
		$this->email->initialize($config);				
		$this->email->from($data["empresa"][0]->usuario, $data["empresa"][0]->titulo);
		$this->email->to($data["empresa"][0]->emailcontato);
		$this->email->subject("Contato - " . $data["empresa"][0]->titulo);
		$this->email->message($mensagem);
		
		if (!$this->email->send()) {
			$data["erro"] = "Erro ao enviar contato. Tente mais tarde.";
		} else {
			$data["sucesso"] = "E-mail enviado com sucesso.";
		}
		
		$data["title"] = "Contato - " . $data["empresa"][0]->titulo;
		$data["description"] =  "Entre em contato com a gente =)";
		$data["image"] = base_url() . "_imagens/logo.png";

		$this->load->vars($data);
		$this->load->view('_paginas/contato');
	}
	
}
?>