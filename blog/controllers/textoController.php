<?php

class TextoController extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    function index() {
		$this->load->model('Texto_model', 'texto');
		$data["textos"] = $this->texto->getAll();
		
		$this->load->vars($data);
		$this->load->view("priv/texto/list");
    }
    
    function addAction() {
		$this->load->view("priv/texto/add");
    }

    function add() {
	    $this->load->model("Texto_model", "texto");
        $insert = array(
            "titulo" => $this->input->post("titulo"),
            "title" => $this->input->post("title"),
            "descricao" => $this->input->post("descricao"),
            "description" => $this->input->post("description"),
            "texto" => $this->input->post("texto"),
            "text" => $this->input->post("text")
        );
		 $result = $this->texto->add_record($insert);

        if ($result > 0) {
				$data["sucesso"] = "Salvo com sucesso.";
				$this->editAction($id, $data);
        }
    }

	function editAction($id, $mensagem) {
		$this->load->model('Texto_model', 'texto');
		$data["texto"] = $this->texto->buscarPorId($id);
		
		$data["erro"] = $mensagem["erro"];
		$data["sucesso"] = $mensagem["sucesso"];
		
		$this->load->vars($data);
		$this->load->view("priv/texto/edit");
	}
	
    function edit() {
        $this->load->model("Texto_model", "texto");
        $id = $this->input->post("id");
		
        $update = array(
            "titulo" => $this->input->post("titulo"),
            "title" => $this->input->post("title"),
            "descricao" => $this->input->post("descricao"),
            "description" => $this->input->post("description"),
            "texto" => $this->input->post("texto"),
            "text" => $this->input->post("text")
        );
		 $result = $this->texto->update($id, $update);

        if ($result > 0) {
				$data["sucesso"] = "Salvo com sucesso.";
				$this->editAction($id, $data);
        }
    }
	
    function delete($id) {
		$this->load->model("Texto_model", "texto");
		$result = $this->texto->delete($id);

		if ($result > 0) {	
			$data["sucesso"] = "Exclu�do com sucesso.";
			$data["texto"] = $this->texto->getAll();
			
			$this->load->vars($data);
			$this->load->view("priv/texto/list");
		}
    }
	
	function uploadImagem($id) {
		$this->load->model('Texto_model', 'texto');
		$delete["delete"] = $this->texto->buscarPorId($id);
		foreach ($delete as $row) { 
			if ($row->imagem) { unlink("./upload/texto/" . $row->imagem);}
		}
		
		$config["upload_path"]   = "./upload/texto/";
		$config["allowed_types"] = "gif|jpg|png";
		$config["file_name"]     = "texto_" . $id . "_" . rand(00, 9999);
		$config["overwrite"]     = TRUE;
		$config["remove_spaces"] = TRUE;		
		$this->load->library("upload", $config);
		
		$result = $this->upload->do_upload("userfile");

        if ($result > 0) {
			$update = array(
				"imagem" => $this->upload->file_name
			);
			$result = $this->texto->update($id, $update);
			
			if ($result > 0) {
				$data["sucesso"] = "Imagem atualizada com sucesso.";
			} else {
				$data["error"] = "Erro ao atualizada imagem no banco de dados.";
			}
		} else {
			$data["error"] = "Erro ao fazer upload da imagem: " . $this->upload->display_errors();
		}
		$this->editAction($id, $data);
	}
    
}
?>