<?php
class Empresa_model extends CI_Model {
    
    function getAll() {
        $query = $this->db->query("select titulo, telefone, endereco, email, facebook, twitter, plus, instagram, youtube, ga, smtp, porta, usuario, senha  from tb_empresa");
        return $query->result();
    }
    
    function update($id, $options = array()) {
        $this->db->where('id', $id);
        $this->db->update('tb_empresa',$options);
        return $this->db->affected_rows();
    }
        
    function buscarPorId($id) {
        $this->db->where('id', $id);
        $query = $this->db->get("tb_empresa");
        return $query->result();
    }
}
?>