<?php

class Usuario_model extends CI_Model {

    private $nome;
    private $login;
    private $senha;
    private $idUsuario;
    private $situacao;

    public function getNome() {
        return $this->nome;
    }

    public function setNome($nome) {
        $this->nome = $nome;
    }

    function getLogin() {
        return $this->login;
    }

    function setLogin($login) {
        $this->login = $login;
    }

    public function getSenha() {
        return $this->senha;
    }

    public function setSenha($senha) {
        $this->senha = $senha;
    }

    public function getIdUsuario() {
        return $this->idUsuario;
    }

    public function setIdUsuario($idUsuario) {
        $this->idUsuario = $idUsuario;
    }

    public function getSituacao() {
        return $this->situacao;
    }

    public function setSituacao($situacao) {
        $this->situacao = $situacao;
    }
	
    function validate() {
        $sql = "select idUsuario, login, situacao, nome from tb_usuario where login = '" . $this->getLogin() . "' and senha = '" . $this->getSenha() . "'";
		$query = $this->db->query($sql);
        return $query->result();
    }

    function logged() {
        $logged = $this->session->userdata('logged');
        if (!isset($logged) || $logged != true) {
            return false;
        }
        return true;
    }

    function nomeLogin() {
        return $this->session->userdata('nome');
    }
    
    function usuarioExiste() {
        $sql = "select idUsuario, login, situacao, nome from tb_usuario where login = '" . $this->session->userdata('login') . "' and senha = '" . $this->session->userdata('senha') . "'";
        $query = $this->db->query($sql);
        if($query->num_rows()>0){
            return TRUE;
        }
        else 
            return FALSE;
    }

    function get_all() {
        $query = $this->db->query("select idUsuario, login, situacao, nome  from tb_usuario");
        return $query->result();
    }

    function get_nome($login) {
        $this->db->where('login', $login);
        $query = $this->db->get("tb_usuario");
        return $query->result();
    }
    
    function getAll() {
        $query = $this->db->query("select idUsuario, login, situacao, nome  from tb_usuario");
        return $query->result();
    }

    function add_record($options = array()) {
        $this->db->insert('tb_usuario', $options);
        return $this->db->insert_id();
    }
    
    function update($options = array(),$id) {
        $this->db->where('idUsuario', $id);
        $this->db->update('tb_usuario', $options);
        return $this->db->affected_rows();
    }
	
    function delete($id) {
        $this->db->where('idUsuario', $id);
        $this->db->delete('tb_usuario',$options);
        return $this->db->affected_rows();
    }

    function buscarPorId($id) {
        $query = $this->db->query("select login, situacao, nome from tb_usuario where idUsuario = $id");
        return $query->result();
    }

}

?>