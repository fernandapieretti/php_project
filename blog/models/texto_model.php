<?php
class Texto_model extends CI_Model {
    
    function getAll() {
        $query = $this->db->query("select id, titulo, imagem from tb_textos ");
        return $query->result();
    }
	
    function add_record($options = array()) {
        $this->db->insert('tb_textos', $options);
        return $this->db->insert_id();
    }
    
    function update($id, $options = array()) {
        $this->db->where('id', $id);
        $this->db->update('tb_textos',$options);
        return $this->db->affected_rows();
    }
    
    function delete($id) {
        $this->db->where('id', $id);
        $this->db->delete('tb_textos',$options);
        return $this->db->affected_rows();
    }
    
    function buscarPorId($id) {
        $this->db->where('id', $id);
        $query = $this->db->get("tb_textos");
        return $query->result();
    }
}
?>