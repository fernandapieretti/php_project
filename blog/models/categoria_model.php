<?php
class Categoria_model extends CI_Model {
    
    function getAll() {
        $query = $this->db->query("select titulo, title from tb_categoria order by titulo asc");
        return $query->result();
    }
	
    function add_record($options = array()) {
        $this->db->insert('tb_categoria', $options);
        return $this->db->insert_id();
    }
    
    function update($id, $options = array()) {
        $this->db->where('id', $id);
        $this->db->update('tb_categoria',$options);
        return $this->db->affected_rows();
    }
    
    function delete($id) {
        $this->db->where('id', $id);
        $this->db->delete('tb_categoria',$options);
        return $this->db->affected_rows();
    }
    
    function buscarPorId($id) {
        $this->db->where('id', $id);
        $query = $this->db->get("tb_categoria");
        return $query->result();
    }
}
?>