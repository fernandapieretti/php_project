<?php
class Post_model extends CI_Model {
    
    function getAll() {
        $query = $this->db->query("select id, titulo, data, imagem, visualizacao from tb_post order by data desc");
        return $query->result();
    }
	
    function add_record($options = array()) {
        $this->db->insert('tb_post', $options);
        return $this->db->insert_id();
    }
    
    function update($id, $options = array()) {
        $this->db->where('id', $id);
        $this->db->update('tb_post',$options);
        return $this->db->affected_rows();
    }
    
    function delete($id) {
        $this->db->where('id', $id);
        $this->db->delete('tb_post',$options);
        return $this->db->affected_rows();
    }
    
    function buscarPorId($id) {
        $this->db->where('id', $id);
        $query = $this->db->get("tb_post");
        return $query->result();
    }
	
    function buscarAtivos() {
        $query = $this->db->query("select p.id, p.titulo, p.data, p.imagem, p.visualizacao, c.id as idcategoria, c.titulo as categoria, c.title as category from tb_post p join tb_categoria c on p.idcategoria = c.id where status = 'ATIVO' order by p.data desc");
        return $query->result();
    }
	
    function buscarMaisLidos() {
        $query = $this->db->query("select p.id, p.titulo, p.data, p.imagem, p.visualizacao, c.id as idcategoria, c.titulo as categoria, c.title as category from tb_post p join tb_categoria c on p.idcategoria = c.id where status = 'ATIVO' order by visualizacao desc limit 0,5");
        return $query->result();
    }
	
    function buscarCategoria($id) {
        $query = $this->db->query("select  id, titulo, data, imagem, visualizacao from tb_post where status = 'ATIVO' and idcategoria = $id order by id asc");
        return $query->result();
    }
	
    function buscarAnterior($data) {
        $query = $this->db->query("select p.id, p.titulo, p.data, p.imagem, p.visualizacao, c.id as idcategoria, c.titulo as categoria, c.title as category from tb_post p join tb_categoria c on p.idcategoria = c.id where status = 'ATIVO' and p.data < '$data' order by p.data desc limit 0,1");
        return $query->result();
    }
	
    function buscarProximo($data) {
        $query = $this->db->query("select p.id, p.titulo, p.data, p.imagem, p.visualizacao, c.id as idcategoria, c.titulo as categoria, c.title as category from tb_post p join tb_categoria c on p.idcategoria = c.id where status = 'ATIVO' and p.data > '$data' order by p.data asc limit 0,1");
        return $query->result();
    }
    
    function buscarPorIdComCategoria($id) {
        $query = $this->db->query("select p.id, p.titulo, p.data, p.imagem, p.visualizacao, c.id as idcategoria, c.titulo as categoria, c.title as category from tb_post p join tb_categoria c on p.idcategoria = c.id where status = 'ATIVO' and p.id = $id order by p.data desc");
        return $query->result();
    }
    
    function buscarAtivosPorCategoria($id) {
        $query = $this->db->query("select p.id, p.titulo, p.data, p.imagem, p.visualizacao, c.id as idcategoria, c.titulo as categoria, c.title as category from tb_post p join tb_categoria c on p.idcategoria = c.id where status = 'ATIVO' and c.id = $id order by p.data desc");
        return $query->result();
    }
	
	function visualizacao($id) {
		$query = $this->db->query("update tb_post set visualizacao = visualizacao + 1 where id = " . $id . " ");
		return $this->db->affected_rows();		
	}
	
	function deletarVisualizacoes() {
		$query = $this->db->query("update tb_post set visualizacao = 0 ");
		return $this->db->affected_rows();		
	}

}
?>