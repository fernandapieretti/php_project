<?php
header("Content-Type: application/xml; charset=UTF-8");
echo '<?xml version="1.0" encoding="UTF-8"?>';
$hoje = date('Y-m-d');

function url($str) {
	$str = strtolower(utf8_decode($str)); $i=1;
	$str = strtr($str, utf8_decode('àáâãäåæçèéêëìíîïñòóôõöøùúûýýÿ'), 'aaaaaaaceeeeiiiinoooooouuuyyy');
	$str = preg_replace("/([^a-z0-9])/",'-',utf8_encode($str));
	while($i>0) $str = str_replace('--','-',$str,$i);
	if (substr($str, -1) == '-') $str = substr($str, 0, -1);
	return $str;
}
?>

<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">
	<url>
		<loc>http://www.catscanada.esy.es/</loc>
		<lastmod><?php echo $hoje;?></lastmod>
		<priority>1.00</priority>
		<changefreq>daily</changefreq>
	</url>
	<? foreach ($categorias as $row) { ?>
	<url>
		<loc>http://www.catscanada.esy.es/categoria/<?=url($row->titulo)?>/<?=$row->id?></loc>
		<lastmod><?php echo $hoje;?></lastmod>
		<priority>0.90</priority>
		<changefreq>daily</changefreq>
	</url>
	<? } ?>
	<? foreach ($posts as $row) { ?>
	<url>
		<loc>http://www.catscanada.esy.es/post/<?=url($row->categoria)?>/<?=url($row->titulo)?>/<?=$row->id?></loc>
		<lastmod><?php echo $hoje;?></lastmod>
		<priority>0.80</priority>
		<changefreq>daily</changefreq>
	</url>
	<? } ?>
	<url>
		<loc>http://www.catscanada.esy.es/texto/nos-5/1</loc>
		<lastmod><?php echo $hoje;?></lastmod>
		<priority>0.70</priority>
		<changefreq>daily</changefreq>
	</url>
</urlset>