<?
if ($this->Usuario_model->logged() == FALSE) {
    redirect('/login/', 'refresh');
}

//Transforma títulos em URL amigáveis
function url($str) {
 $str = strtolower(utf8_decode($str)); $i=1;
    $str = strtr($str, utf8_decode('àáâãäåæçèéêëìíîïñòóôõöøùúûýýÿ'), 'aaaaaaaceeeeiiiinoooooouuuyyy');
    $str = preg_replace("/([^a-z0-9])/",'-',utf8_encode($str));
    while($i>0) $str = str_replace('--','-',$str,$i);
    if (substr($str, -1) == '-') $str = substr($str, 0, -1);
    return $str;
}
?>

<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<meta charset="utf-8" />
		<meta name="robots" content="noindex,nofollow" />
		<title>Blog</title>
		<link rel="stylesheet" type="text/css" href="<?=base_url()?>css/styles.min.css" />
		<link rel="shortcut icon" href="<?=base_url()?>_imagens/favico.ico" />
	</head>

    <div id="wrapper">

        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <a class="navbar-brand" href="<?=base_url()?>principal/arearestrita">Olá <?= $this->session->userdata("nome") ?>!</a>
            </div>
            <ul class="nav navbar-top-links navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="<?=base_url()?>usuarioController/editarUsuarioAction/1"><i class="fa fa-user fa-fw"></i> Alterar senha</a></li>
                        <li class="divider"></li>
                        <li><a href="<?=base_url()?>login/login/logoff"><i class="fa fa-sign-out fa-fw"></i> Sair</a></li>
                    </ul>
                </li>
            </ul>
        </nav>

        <nav class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="side-menu">
					<li class="sidebar-search"><a href="<?=base_url()?>principal/arearestrita"><img src="<?= base_url() ?>_imagens/logo.png" alt="BLOG" title="BLOG" width="100%" /></a></li>
					<li><a href="<?=base_url()?>postController/">Post</a></li>
					<li><a href="<?=base_url()?>categoriaController/">Categorias</a></li>
					<li><a href="<?=base_url()?>textoController/">Textos</a></li>
					<li><a href="<?=base_url()?>empresaController/editarEmpresaAction/1">Sobre </a></li>
                </ul>
            </div>
        </nav>
	   
	   