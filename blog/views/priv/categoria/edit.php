<? $this->load->view('priv/_inc/superior'); ?>

<div id="page-wrapper">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Categorias</h1>
			
			<div class="panel panel-default">
				<div class="panel-heading"><a href="<?= base_url() ?>principal/arearestrita">Principal</a> &raquo; <a href="<?= BASE_URL(); ?>categoriaController/"> Categorias</a> &raquo; Editar</div>
			</div>
				
			<?= $sucesso != "" ? '<div class="alert alert-success"> ' . $sucesso . ' </div>' : "" ?>
			<?= $erro != "" ? '<div class="alert alert-danger"> ' . $erro . ' </div>' : "" ?>

			<? foreach ($categoria as $row) { ?>
				<form method="post" action="<?= BASE_URL(); ?>categoriaController/edit">			
					<input type="hidden" name="id" id="id" value="<?= $row->id ?>"/>
					<div class="form-group">
						<label>Título</label><br />
						<input type="text" name="titulo" id="titulo" class="form-control" value="<?=$row->titulo?>"/>
					</div>
					<div class="form-group">
						<label>Title</label><br />
						<input type="text" name="title" id="title" class="form-control" value="<?=$row->title?>"/>
					</div>
					<div class="form-group">
						<input type="button" value="Voltar" class="btn btn-default" onClick="location.href='<?= base_url() ?>categoriaController'" />
						<input type="submit" class="btn btn-success" name="btSalvarArtigo" value="Salvar" />
					</div>
				</form>
			<? } ?>
		</div>
	</div>
</div>

<? $this->load->view('priv/_inc/inferior'); ?>
