<?
$this->load->view('priv/_inc/superior');
?>

<script>
	function confirmaExcluir(id) {
		var r=confirm("Deseja excluir este item?")
		if (r==true) { location.href = "<?= base_url() ?>categoriaController/delete/" + id; }
	}
</script>

<div id="page-wrapper">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header"> Categorias <a href="<?=base_url()?>categoriaController/addAction" style="float:right" class="btn btn-success">Cadastrar</a> </h1>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading"><a href="<?= base_url() ?>principal/arearestrita">Principal</a> &raquo; Categorias </div>
			</div>
			<?= $sucesso != "" ? '<div class="alert alert-success"> ' . $sucesso . ' </div>' : "" ?>
			<?= $erro != "" ? '<div class="alert alert-danger"> ' . $erro . ' </div>' : "" ?>
			<table id="dataTables-example" class="table table-striped table-bordered table-hover">
				<thead>
					<th>Título</th>
					<th width="120">Ações</th>
				</thead>
				<? foreach ($categorias as $row) { ?>
				<tr>
					<td><?= $row->titulo ?></td>
					<td align="center"><a href="<?= base_url() ?>categoriaController/editAction/<?= $row->id ?>">Editar</a> | <a onclick="confirmaExcluir(<?= $row->id ?>)">Excluir</a></td>
				</tr>
				<? } ?>
			</table>
		</div>
	</div>
</div>

<? $this->load->view('priv/_inc/inferior'); ?>
