<? $this->load->view('priv/_inc/superior'); ?>

<div id="page-wrapper">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Categorias</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading"><a href="<?= base_url() ?>principal/arearestrita">Principal</a> &raquo;  <a href="<?= BASE_URL(); ?>categoriaController/">Categorias</a> &raquo; Cadastrar</div>
			</div>
			<form method="post" action="add">
				<div class="form-group">
					<label>Título</label><br />
					<input type="text" name="titulo" id="titulo" class="form-control"/>
				</div>
				<div class="form-group">
					<label>Title</label><br />
					<input type="text" name="title" id="title" class="form-control" />
				</div>
				<div class="form-group">
					<input type="button" value="Voltar" class="btn btn-default" onclick="location.href='<?= base_url() ?>categoriaController'" />
					<input type="submit" value="Salvar" class="btn btn-success" />
				</div>
			</form>
		</div>
	</div>
</div>

<? $this->load->view('priv/_inc/inferior'); ?>
