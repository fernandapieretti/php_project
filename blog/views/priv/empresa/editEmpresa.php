<?
$this->load->view('priv/_inc/superior');
?>

<div id="page-wrapper">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Sobre</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading"><a href="<?= base_url() ?>principal/arearestrita">Principal</a> &raquo; Sobre</div>
			</div>
			<?= $sucesso != "" ? '<div class="alert alert-success"> ' . $sucesso . ' </div>' : "" ?>
			<?= $erro != "" ? '<div class="alert alert-danger"> ' . $erro . ' </div>' : "" ?>

			<? foreach ($empresa as $row) { ?>
			<form method="post" action="<?= BASE_URL(); ?>empresaController/editEmpresa">			
				<input type="hidden" name="id" id="id" value="<?= $row->id ?>"/>
				<div class="form-group">
					<label>Nome</label><br />
					<input type="text" name="titulo" id="titulo" value="<?= $row->titulo ?>" class="form-control" />
				</div>
				<div class="form-group">
					<label>E-mail</label><br />
					<input type="text" name="email" id="email" value="<?= $row->email ?>" class="form-control" />
				</div>
				<div class="form-group">
					<label>Telefone</label><br />
					<input type="text" name="telefone" id="telefone" value="<?= $row->telefone ?>" class="form-control" />
				</div>
				<div class="form-group">
					<label>Endereço</label><br />
					<input type="text" name="endereco" id="endereco" value="<?= $row->endereco ?>" class="form-control" />
				</div>
				<div class="form-group">
					<label>Facebook</label><br />
					<input type="text" name="facebook" id="facebook" value="<?= $row->facebook ?>" class="form-control" />
				</div>
				<div class="form-group">
					<label>G+</label><br />
					<input type="text" name="plus" id="plus" value="<?= $row->plus ?>" class="form-control" />
				</div>
				<div class="form-group">
					<label>Instagram</label><br />
					<input type="text" name="instagram" id="instagram" value="<?= $row->instagram ?>" class="form-control" />
				</div>
				<div class="form-group">
					<label>Youtube</label><br />
					<input type="text" name="youtube" id="youtube" value="<?= $row->youtube ?>" class="form-control" />
				</div>
				<div class="form-group">
					<label>Twitter</label><br />
					<input type="text" name="twitter" id="twitter" value="<?= $row->twitter ?>" class="form-control" />
				</div>
				<div class="form-group">
					<label>Código do Google Analytics</label><br />
					<textarea name="ga" id="ga" class="form-control" style="height:200px"><?= $row->ga ?></textarea>
				</div>
				<div class="form-group">
					<label>Servidor para envio de e-mail: SMTP</label><br />
					<input type="text" name="smtp" id="smtp" value="<?= $row->smtp ?>" class="form-control" />
				</div>
				<div class="form-group">
					<label>Servidor para envio de e-mail: Porta</label><br />
					<input type="text" name="porta" id="porta" value="<?= $row->porta ?>" class="form-control" />
				</div>
				<div class="form-group">
					<label>Servidor para envio de e-mail: E-mail</label><br />
					<input type="text" name="usuario" id="usuario" value="<?= $row->usuario ?>" class="form-control" />
				</div>
				<div class="form-group">
					<label>Servidor para envio de e-mail: Senha</label><br />
					<input type="password" name="senha" id="senha" value="<?= $row->senha ?>" class="form-control" />
				</div>
				<div class="form-group">
					<input type="button" value="Voltar" class="btn btn-default" onClick="location.href='<?= base_url() ?>principal/arearestrita'" />
					<input type="submit" class="btn btn-success" name="btSalvarEmpresa" value="Salvar Empresa" />
				</div>
			</form>
		<? } ?>
	</div>
</div>
<?
$this->load->view('priv/_inc/inferior');
?>
