<?
$this->load->view('priv/_inc/superior-anunciante');
?>

<div id="page-wrapper">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Alterar senha</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">

			<?= $sucesso != "" ? '<div class="alert alert-success"> ' . $sucesso . ' </div>' : "" ?>
			<?= $erro != "" ? '<div class="alert alert-danger"> ' . $erro . ' </div>' : "" ?>

			<? foreach ($usuario as $row) { ?>
			<form method="post" action="<?= BASE_URL(); ?>usuarioController/editUsuarioCadastro">
				<input type="hidden" name="idcadastro" id="idcadastro" value="<?= $row->idcadastro ?>"/>
				<input type="hidden" name="id" id="id" value="<?= $row->idUsuario ?>"/>
				<div class="form-group">
					<label>Nome</label><br />
					<input type="text" name="nome" id="nome" value="<?= $row->nome ?>" class="form-control" />
				</div>
				<div class="form-group">
					<label>Login</label><br />
					<input type="text" name="login" id="login" value="<?= $row->login ?>" class="form-control" />
				</div>
				<div class="form-group">
					<label>Senha</label><br />
					<input type="text" name="senha" id="senha" value="<?= $row->senha ?>" class="form-control" />
				</div>
				<div class="form-group">
					<input type="submit" class="btn btn-success" name="btSalvarUsuario" value="Alterar" />
				</div>
			</form>
			<? } ?>
		</div>
	</div>
</div>
<?
$this->load->view('priv/_inc/inferior');
?>
