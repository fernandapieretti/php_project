<? $this->load->view('priv/_inc/superior'); ?>

<script>
	function confirmaExcluirGaleria(id) {
		var r=confirm("Deseja excluir este item?")
		if (r==true) { location.href = "<?= base_url() ?>textoController/deleteGaleria/" + id; }
	}
</script>

<div id="page-wrapper">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Textos</h1>
			
			<div class="panel panel-default">
				<div class="panel-heading"><a href="<?= base_url() ?>principal/arearestrita">Principal</a> &raquo; <a href="<?= BASE_URL(); ?>textoController/"> Textos </a> &raquo; Editar</div>
			</div>
				
			<?= $sucesso != "" ? '<div class="alert alert-success"> ' . $sucesso . ' </div>' : "" ?>
			<?= $erro != "" ? '<div class="alert alert-danger"> ' . $erro . ' </div>' : "" ?>

			<? foreach ($texto as $row) { ?>
				<form method="post" action="<?= BASE_URL(); ?>textoController/edit">			
					<input type="hidden" name="id" id="id" value="<?= $row->id ?>"/>
					<div class="form-group">
						<label>Título</label><br />
						<input type="text" name="titulo" id="titulo" value="<?= $row->titulo ?>" class="form-control" />
					</div>
					<div class="form-group">
						<label>Title</label><br />
						<input type="text" name="title" id="title" value="<?= $row->title ?>" class="form-control" />
					</div>
					<div class="form-group">
						<label>Descrição</label><br />
						<textarea name="descricao" id="descricao" class="form-control"><?= $row->descricao ?></textarea>
					</div>
					<div class="form-group">
						<label>Description</label><br />
						<textarea name="description" id="description" class="form-control"><?= $row->description ?></textarea>
					</div>
					<div class="form-group">
						<label>Texto</label><br />
						<textarea name="texto" id="texto" class="form-control ckeditor"><?= $row->texto ?></textarea>
					</div>
					<div class="form-group">
						<label>Text</label><br />
						<textarea name="text" id="text" class="form-control ckeditor"><?= $row->text ?></textarea>
					</div>
					<div class="form-group">
						<input type="button" value="Voltar" class="btn btn-default" onClick="location.href='<?= base_url() ?>textoController'" />
						<input type="submit" class="btn btn-success" name="btSalvarTexto" value="Salvar" />
					</div>
				</form>
				<br /><br />
				
				<h1 class="page-header">Imagem</h1>
				
				<form action="<?=base_url()?>textoController/uploadImagem/<?= $row->id ?>" method="post" enctype="multipart/form-data">
					<div class="form-group">
						<? if ($row->imagem) { ?>
							<img src="<?=base_url()?>upload/texto/<?=$row->imagem?>" style="height:200px;margin-bottom:10px" /> <br>
						<? } ?>
						<input type="file" class="form-control" name="userfile" id="userfile" style="float:left;width:200px;margin-right:10px" />
						<input class="btn btn-success" type="submit" name="enviar" value="Salvar" />
					</div>
				</form>				
			<? } ?>
		</div>
	</div>
</div>

<? $this->load->view('priv/_inc/inferior'); ?>
