<?
$this->load->view('priv/_inc/superior');
?>

<div id="page-wrapper">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Textos</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading"><a href="<?= base_url() ?>principal/arearestrita">Principal</a> &raquo; Textos </div>
			</div>
			<?= $sucesso != "" ? '<div class="alert alert-success"> ' . $sucesso . ' </div>' : "" ?>
			<?= $erro != "" ? '<div class="alert alert-danger"> ' . $erro . ' </div>' : "" ?>
			<table id="dataTables-example" class="table table-striped table-bordered table-hover">
				<thead>
					<th width="100">Imagem</th>
					<th>Título</th>
					<th width="120">Ações</th>
				</thead>
				<? foreach ($textos as $row) { ?>
				<tr>
					<td><img src="<?=base_url()?>upload/texto/<?= $row->imagem ?>" width="100" /></td>
					<td><?= $row->titulo ?></td>
					<td align="center"><a href="<?= base_url() ?>textoController/editAction/<?= $row->id ?>">Editar</a></td>
				</tr>
				<? } ?>
			</table>
		</div>
	</div>
</div>

<? $this->load->view('priv/_inc/inferior'); ?>
