<? $this->load->view('priv/_inc/superior'); ?>

<script>
	function confirmaExcluirGaleria(id) {
		var r=confirm("Deseja excluir este item?")
		if (r==true) { location.href = "<?= base_url() ?>postController/deleteGaleria/" + id; }
	}
</script>

<div id="page-wrapper">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Post</h1>
			
			<div class="panel panel-default">
				<div class="panel-heading"><a href="<?= base_url() ?>principal/arearestrita">Principal</a> &raquo; <a href="<?= BASE_URL(); ?>postController/"> Post </a> &raquo; Editar</div>
			</div>
			
			<?= $sucesso != "" ? '<div class="alert alert-success"> ' . $sucesso . ' </div>' : "" ?>
			<?= $error != "" ? '<div class="alert alert-danger"> ' . $error . ' </div>' : "" ?>

			<? foreach ($post as $row) { ?>
				<form method="post" action="<?= BASE_URL(); ?>postController/edit">			
					<input type="hidden" name="id" id="id" value="<?= $row->id ?>"/>
					<div class="row">
						<div class="col-lg-4">
							<div class="form-group">
								<label>Visualizações</label><br />
								<input type="text" name="visualizacao" id="visualizacao" class="form-control" value="<?=$row->visualizacao?>" />
							</div>
						</div>
						<div class="col-lg-4">
							<div class="form-group">
								<label>Data</label><br />
								<input type="text" name="data" id="data" class="form-control" value="<?=implode("/",array_reverse(explode("-", $row->data)))?>" />
							</div>
						</div>
						<div class="col-lg-4">
							<div class="form-group">
								<label>Categoria</label><br />
								<select name="idcategoria" id="idcategoria" class="form-control">
									<option value=""></option>
									<? foreach ($categorias as $cat) { ?>
										<option <?=$row->idcategoria == $cat->id ? "selected" : "" ?> value="<?=$cat->id?>"><?=$cat->titulo?></option>
									<? } ?>
								</select>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label>Título</label><br />
						<input type="text" name="titulo" id="titulo" class="form-control" value="<?=$row->titulo?>" />
					</div>
					<div class="form-group">
						<label>Title</label><br />
						<input type="text" name="title" id="title" class="form-control" value="<?=$row->title?>" />
					</div>
					<div class="form-group">
						<label>Palavras</label><br />
						<textarea name="palavras" id="palavras" class="form-control"><?=$row->palavras?></textarea>
					</div>
					<div class="form-group">
						<label>Descrição</label><br />
						<textarea name="descricao" id="descricao" class="form-control"><?=$row->descricao?></textarea>
					</div>
					<div class="form-group">
						<label>Description</label><br />
						<textarea name="description" id="description" class="form-control"><?=$row->description?></textarea>
					</div>
					<div class="form-group">
						<label>Resumo</label><br />
						<textarea name="resumo" id="resumo" class="form-control"><?=$row->resumo?></textarea><br>
					</div>
					<div class="form-group">
						<label>Summary</label><br />
						<textarea name="summary" id="summary" class="form-control"><?=$row->summary?></textarea><br>
					</div>
					<div class="form-group">
						<label>Texto</label><br />
						<textarea name="texto" id="texto" class="form-control ckeditor"><?=$row->texto?></textarea><br>
					</div>
					<div class="form-group">
						<label>Text</label><br />
						<textarea name="text" id="text" class="form-control ckeditor"><?=$row->text?></textarea><br>
					</div>
					<div class="form-group">
						<label>Autor</label><br />
						<select name="autor" id="autor" class="form-control">
							<option <?=$row->autor == "1" ? "selected" : "" ?> value="1">Fernanda</option>
							<option <?=$row->autor == "2" ? "selected" : "" ?> value="2">Igor</option>
						</select>
					</div>
					<div class="form-group">
						<label>Status</label><br />
						<select name="status" id="status" class="form-control">
							<option <?=$row->status == "ATIVO" ? "selected" : "" ?> value="ATIVO">ATIVO</option>
							<option <?=$row->status == "INATIVO" ? "selected" : "" ?> value="INATIVO">INATIVO</option>
						</select>
					</div>
					<div class="form-group">
						<input type="button" value="Voltar" class="btn btn-default" onClick="location.href='<?= base_url() ?>postController'" />
						<input type="submit" class="btn btn-success" name="btSalvarArtigo" value="Salvar" />
					</div>
				</form>
				<br /><br />
								
				<h1 class="page-header">Imagem</h1>				
				<form action="<?=base_url()?>postController/uploadImagem/<?= $row->id ?>" method="post" enctype="multipart/form-data">
					<div class="form-group">					
						<? if ($row->imagem) { ?> <img src="<?=base_url()?>upload/post/<?=$row->imagem?>" style="width:200px;margin-bottom:10px" /> <br> <? } ?>
						<input type="file" class="form-control" name="userfile" id="userfile" style="float:left;width:200px;margin-right:10px" />
						<input class="btn btn-success" type="submit" name="enviar" value="Salvar" />
					</div>
				</form>				
			<? } ?>
		</div>
	</div>
</div>

<? $this->load->view('priv/_inc/inferior'); ?>
