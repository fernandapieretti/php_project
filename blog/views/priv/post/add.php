<? $this->load->view('priv/_inc/superior'); ?>

<div id="page-wrapper">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Post</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading"><a href="<?= base_url() ?>principal/arearestrita">Principal</a> &raquo;  <a href="<?= BASE_URL(); ?>postController/">Post</a> &raquo; Cadastrar</div>
			</div>
			<form method="post" action="add">
				<div class="row">
					<div class="col-lg-4">
						<div class="form-group">
							<label>Data</label><br />
							<input type="text" name="data" id="data" class="form-control" value="<?=date("d/m/Y")?>"/>
						</div>
					</div>
					<div class="col-lg-8">
						<div class="form-group">
							<label>Categoria</label><br />
							<select name="idcategoria" id="idcategoria" class="form-control">
								<option value=""></option>
								<? foreach ($categorias as $cat) { ?>
								<option value="<?=$cat->id?>"><?=$cat->titulo?></option>
								<? } ?>
							</select>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label>Título</label><br />
					<input type="text" name="titulo" id="titulo" class="form-control"/>
				</div>
				<div class="form-group">
					<label>Title</label><br />
					<input type="text" name="title" id="title" class="form-control"/>
				</div>
				<div class="form-group">
					<label>Palavras <small>[ termos separados por vírgula ]</small></label><br />
					<textarea name="palavras" id="palavras" class="form-control"></textarea>
				</div>
				<div class="form-group">
					<label>Descrição</label><br />
					<textarea name="descricao" id="descricao" class="form-control"></textarea>
				</div>
				<div class="form-group">
					<label>Description</label><br />
					<textarea name="description" id="description" class="form-control"></textarea>
				</div>
				<div class="form-group">
					<label>Resumo </label><br />
					<textarea name="resumo" id="resumo" class="form-control"></textarea><br>
				</div>
				<div class="form-group">
					<label>Summary </label><br />
					<textarea name="summary" id="summary" class="form-control"></textarea><br>
				</div>
				<div class="form-group">
					<label>Text</label><br />
					<textarea name="texto" id="texto" class="form-control ckeditor"></textarea><br>
				</div>
				<div class="form-group">
					<label>Texto</label><br />
					<textarea name="text" id="text" class="form-control ckeditor"></textarea><br>
				</div>
				<div class="form-group">
					<label>Autor</label><br />
					<select name="autor" id="autor" class="form-control">
						<option value="1">Fernanda</option>
						<option value="2">Igor</option>
					</select>
				</div>
				<div class="form-group">
					<label>Status</label><br />
					<select name="status" id="status" class="form-control">
						<option value="ATIVO">ATIVO</option>
						<option value="INATIVO">INATIVO</option>
					</select>
				</div>
				<div class="form-group">
					<input type="button" value="Voltar" class="btn btn-default" onclick="location.href='<?= base_url() ?>postController'" />
					<input type="submit" value="Salvar" class="btn btn-success" />
				</div>
			</form>
		</div>
	</div>
</div>

<? $this->load->view('priv/_inc/inferior'); ?>
