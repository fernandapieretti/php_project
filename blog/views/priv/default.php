<? $this->load->view('priv/_inc/superior');?>

<div id="page-wrapper">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Principal</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading"> Utilize o menu à esquerda para administrar seu site. </div>
				<div id="container" style="width:100%;height:400px;margin:40px 0"></div>
				
				<script>
					$(function () {
						$('#container').highcharts({
							chart: {
								type: 'column'
							},
							title: {
								text: 'Posts mais lidos'
							},
							xAxis: {
								categories: [
									'Total'
								]
							},
							yAxis: {
								min: 0,
								title: {
									text: ''
								}
							},
							exporting: {
							  enabled: false
							},
							 credits: {
							  enabled: false
							},
							plotOptions: {
								column: {
									pointPadding: 0.2,
									borderWidth: 0
								}
							},
							series: [{
								name: '<?=$maislidos[0]->titulo?>',
								data: [<?=$maislidos[0]->visualizacao?>]

							}, {
								name: '<?=$maislidos[1]->titulo?>',
								data: [<?=$maislidos[1]->visualizacao?>]

							}, {
								name: '<?=$maislidos[2]->titulo?>',
								data: [<?=$maislidos[2]->visualizacao?>]

							}, {
								name: '<?=$maislidos[3]->titulo?>',
								data: [<?=$maislidos[3]->visualizacao?>]

							}, {
								name: '<?=$maislidos[4]->titulo?>',
								data: [<?=$maislidos[4]->visualizacao?>]

							}]
						});
					});
				</script>
			
			</div>
		</div>
	</div>
</div>

<? $this->load->view('priv/_inc/inferior'); ?>
