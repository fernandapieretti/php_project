<?

//Transforma títulos em URL amigáveis
function url($str) {
 $str = strtolower(utf8_decode($str)); $i=1;
    $str = strtr($str, utf8_decode('àáâãäåæçèéêëìíîïñòóôõöøùúûýýÿ'), 'aaaaaaaceeeeiiiinoooooouuuyyy');
    $str = preg_replace("/([^a-z0-9])/",'-',utf8_encode($str));
    while($i>0) $str = str_replace('--','-',$str,$i);
    if (substr($str, -1) == '-') $str = substr($str, 0, -1);
    return $str;
}

//Formata data
function formatarData($param) {
	$data = explode('-', $param);
	
	if ($_SESSION["lingua"] == "ingles") {
		//Semana
		$diasemana = date("w", mktime(0,0,0,$data[1],$data[2],$data[0]) );
		switch($diasemana) {
			case"0": $dia_semana = "Sunday"; break;
			case"1": $dia_semana = "Monday"; break;
			case"2": $dia_semana = "Tuesday"; break;
			case"3": $dia_semana = "Wednesday"; break;
			case"4": $dia_semana = "Thrusday"; break;
			case"5": $dia_semana = "Friday"; break;
			case"6": $dia_semana = "Saturday"; break;
		}
		
		//mês
		switch ($data[1]) {
			case "1" : $mes = "Jan"; break;
			case "2" : $mes = "Feb"; break;
			case "3" : $mes = "Mar"; break;
			case "4" : $mes = "Apr"; break;
			case "5" : $mes = "May"; break;
			case "6" : $mes = "Jun"; break;
			case "7" : $mes = "Jul"; break;
			case "8" : $mes = "Aug"; break;
			case "9" : $mes = "Sep"; break;
			case "10" : $mes = "Oct"; break;
			case "11" : $mes = "Nov"; break;
			case "12" : $mes = "Dec"; break;
		}
		return $dia_semana . ", " . $data[2] . "th " . $mes . " " . $data[0];
	} else {
		//Semana
		$diasemana = date("w", mktime(0,0,0,$data[1],$data[2],$data[0]) );
		switch($diasemana) {
			case"0": $dia_semana = "Domingo"; break;
			case"1": $dia_semana = "Segunda"; break;
			case"2": $dia_semana = "Terca"; break;
			case"3": $dia_semana = "Quarta"; break;
			case"4": $dia_semana = "Quinta"; break;
			case"5": $dia_semana = "Sexta"; break;
			case"6": $dia_semana = "Sabado"; break;
		}
		
		//mês
		switch ($data[1]) {
			case "1" : $mes = "Janeiro"; break;
			case "2" : $mes = "Fevereiro"; break;
			case "3" : $mes = "Março"; break;
			case "4" : $mes = "Abril"; break;
			case "5" : $mes = "Maio"; break;
			case "6" : $mes = "Junho"; break;
			case "7" : $mes = "Julho"; break;
			case "8" : $mes = "Agosto"; break;
			case "9" : $mes = "Setembro"; break;
			case "10" : $mes = "Outubro"; break;
			case "11" : $mes = "Novembro"; break;
			case "12" : $mes = "Dezembro"; break;
		}
		return $dia_semana . ", " . $data[2] . " de " . $mes . " de " . $data[0];
	}
}

//Saudação
function retornaSaudacao() {
	$hora = date("H");
	if (($hora >=6) && ($hora <=11)) $saudacao = "bom dia";
	if (($hora >11) && ($hora <=18)) $saudacao = "boa tarde";
	if (($hora >18) && ($hora <=24)) $saudacao = "boa noite";
	if (($hora >24) && ($hora <6)) $saudacao = "boa noite";
	return $saudacao;
}

function retornaPeriodo() {
	$hora = date("H");
	if (($hora >=6) && ($hora <=11)) $saudacao = "manhã";
	if (($hora >11) && ($hora <=18)) $saudacao = "tarde";
	if (($hora >18) && ($hora <=24)) $saudacao = "noite";
	if (($hora >24) && ($hora <6)) $saudacao = "noite";
	return $saudacao;
}

?>