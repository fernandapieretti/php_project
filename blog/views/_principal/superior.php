<?
session_start();
date_default_timezone_set('America/Campo_Grande');
include_once("config.php");
?>

<!DOCTYPE html>
<html lang="pt-br">
	
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="robots" content="noindex,nofollow" />

	<title><?=$title?></title>
	<meta name="description" content='<?=$description?>' />
	<meta name="keywords" content="<?=$keywords?>" />
	<meta name="viewport" content="width=device-width" />
	<link rel="shortcut icon" href="<?=base_url()?>_imagens/favico.ico" />	

	<meta property="og:type" content="article" />
	<meta property="og:url" content="http://<?=$_SERVER['SERVER_NAME'] . $_SERVER ['REQUEST_URI']?>" />
	<meta property="og:title" content='<?=$title?>' />
	<meta property="og:description" content='<?=$description?>' />
	<meta property="og:image" content='<?=$image?>' />
	<meta property="fb:app_id" content="" />

	<link href='https://fonts.googleapis.com/css?family=Josefin+Sans:400,600,700' rel='stylesheet' type='text/css' />
	<link href='https://fonts.googleapis.com/css?family=Ubuntu:400,300,600,800,900' rel='stylesheet' type='text/css' />
	<link rel='stylesheet' href='<?=base_url()?>_estilos/style.min.css' type='text/css' media='all' />
</head>

<body class="<?=$class?>">
	
	<? $this->load->view("_principal/analyticstracking.php"); ?>	
	<script>
		dataLayer.push({
			'categoria': '<?=$dataLayerCategoria?>',
			'area': '<?=$dataLayerArea?>',
			'post': '<?=$dataLayerPost?>'
		});
	</script>
	
	<nav>
		<div id="nav">			
			<div class="container">
				<div class="row">
					<div class="col-md-3 col-sm-3">
						<a href="<?=base_url()?>" class="logo"><img src="<?=base_url()?>_imagens/logo.png" alt="" /></a>
						<img src="<?=base_url()?>_imagens/menu.png" alt="" id="menuDropDown" />
					</div>
					<div class="col-md-9 col-sm-9">
						<ul>
							<li><a href="<?=base_url()?>" data-post="Home" class="item-menu">Home</a></li>
							<? foreach ($categorias as $row) { ?>
							<li><a href="<?=base_url()?>categoria/<?=url($row->titulo)?>/<?=$row->id?>" data-post="<?=$row->titulo?>" class="item-menu"><?=$_SESSION["lingua"] == "ingles" ? $row->title : $row->titulo ?></a></li>
							<? } ?>
							<li><a href="<?=base_url()?>texto/<?=url($sobre[0]->titulo)?>/<?=$sobre[0]->id?>" data-post="<?=$sobre[0]->titulo?>" class="item-menu"><?=$_SESSION["lingua"] == "ingles" ? $sobre[0]->title : $sobre[0]->titulo ?></a></li>
							<li class="lang">
								<a href="<?=base_url()?>ler-em-ingles" data-post="Ler em Inglês" class="item-menu"><img src="<?=base_url()?>_imagens/usa.png" alt="" /></a>
								<a href="<?=base_url()?>ler-em-portugues" data-post="Ler em Português" class="item-menu"><img src="<?=base_url()?>_imagens/brasil.png" alt="" /></a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</nav>
	
	<header>
		<div id="header">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<p><?=$_SESSION["lingua"] == "ingles" ? "The adventures of three brazilian little cats <br> and its owners in Vancouver." : "As aventuras de três gatinhos brasileiros <br> e seus donos em Vancouver." ?></p>
					</div>
				</div>
			</div>
		</div>
	</header>
	
	<input type="hidden" value="<?=retornaSaudacao()?>" id="valorSaudacaoLeitor" />
	<input type="hidden" value="<?=retornaPeriodo()?>" id="valorPeriodoDia" />
	<input type="hidden" value="<?=date("d/m/Y")?>" id="hoje" />
	