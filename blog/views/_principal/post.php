	<section>
		<div id="page">
			<div id="posts">
				
				<div class="container">
					<div class="row">
						<div class="col-md-8">
							
							<? foreach ($post as $row) { ?>
							<article class="post">
								<a class="entry-category-post" href="<?=base_url()?>categoria/<?=url($row->categoria)?>/<?=$row->idcategoria?>" data-post="<?=$row->titulo?>"><?=$_SESSION["lingua"] == "ingles" ? $row->category : $row->categoria ?></a>
								<h1 class="entry-title"><?=$_SESSION["lingua"] == "ingles" ? $row->title : $row->titulo ?></h1>
								<img src="<?=base_url()?>upload/post/<?=$row->imagem?>" alt="" class="entry-imgpost" />
								
								<p class="entry-itens-post">
									<span class="entry-data"><img class="icon" src="<?=base_url()?>_imagens/icon-calendar.png" alt="" /> <?=formatarData($row->data)?></span>
									<span class="entry-share">
										<a href="http://www.facebook.com/sharer/sharer.php?u=<?=base_url()?>post/<?=url($row->categoria)?>/<?=url($row->titulo)?>/<?=$row->id?>" class="linkShare" data-post="<?=$row->titulo?>"><img src="<?=base_url()?>_imagens/face.png" alt="" /></a>
										<a href="http://twitter.com/intent/tweet?text=<?=base_url()?>post/<?=url($row->categoria)?>/<?=url($row->titulo)?>/<?=$row->id?>" class="linkShare" data-post="<?=$row->titulo?>"><img src="<?=base_url()?>_imagens/twitter.png" alt="" /></a>
										<a href="https://plus.google.com/share?url=<?=base_url()?>post/<?=url($row->categoria)?>/<?=url($row->titulo)?>/<?=$row->id?>" class="linkShare" data-post="<?=$row->titulo?>"><img src="<?=base_url()?>_imagens/plus.png" alt="" /></a>
									</span>
								</p>
								
								<div class="entry-content">
									<div class="entry-text">
										<p align="center"><b><i><?=$_SESSION["lingua"] == "ingles" ? $row->description : $row->descricao ?></i></b></p>
										<?=$_SESSION["lingua"] == "ingles" ? $row->text : $row->texto ?>
									</div>
								</div>
							</article>	
							<? } ?>
							
							<div style="display:table">
								<article class="autor">
									<img src="<?=base_url()?>upload/texto/<?=$autor[0]->imagem?>" alt="" />
									<p style="color:#51ab6d"><?=$_SESSION["lingua"] == "ingles" ? "AUTHOR" : "AUTOR" ?></p>
									<h3><?=$autor[0]->titulo?></h3>
									<p><p><?=$_SESSION["lingua"] == "ingles" ? $autor[0]->text : $autor[0]->texto ?></p>
								</article>
							</div>
							
							<div style="display:table">
								<? if ($anterior) { ?>
								<article class="post anterior">
									<h3><?=$_SESSION["lingua"] == "ingles" ? "PREVIOUS POST" : "POST ANTERIOR" ?></h3>
									<a href="<?=base_url()?>post/<?=url($anterior[0]->categoria)?>/<?=url($anterior[0]->titulo)?>/<?=$anterior[0]->id?>" class="imgAnterior" data-post="<?=$anterior[0]->titulo?>"><span></span><img src="<?=base_url()?>upload/post/<?=$anterior[0]->imagem?>" alt="" class="principal" /></a>
									<h2 class="entry-title"><a href="<?=base_url()?>post/<?=url($anterior[0]->categoria)?>/<?=url($anterior[0]->titulo)?>/<?=$anterior[0]->id?>" data-post="<?=$anterior[0]->titulo?>"><?=$_SESSION["lingua"] == "ingles" ? $anterior[0]->title : $anterior[0]->titulo ?></a></h2>
								</article>
								<? } ?>
								<? if ($proximo) { ?>
								<article class="post proximo">
									<h3><?=$_SESSION["lingua"] == "ingles" ? "NEXT POST" : "PRÓXIMO POST" ?></h3>
									<a href="<?=base_url()?>post/<?=url($proximo[0]->categoria)?>/<?=url($proximo[0]->titulo)?>/<?=$proximo[0]->id?>" class="imgProximo" data-post="<?=$proximo[0]->titulo?>"><span></span><img src="<?=base_url()?>upload/post/<?=$proximo[0]->imagem?>" alt="" class="principal" /></a>
									<h2 class="entry-title"><a href="<?=base_url()?>post/<?=url($proximo[0]->categoria)?>/<?=url($proximo[0]->titulo)?>/<?=$proximo[0]->id?>" data-post="<?=$proximo[0]->titulo?>"><?=$_SESSION["lingua"] == "ingles" ? $proximo[0]->title : $proximo[0]->titulo ?></a></h2>
								</article>
								<? } ?>
							</div>
							
							<div class="post">
								<h2 class="entry-title"><?=$_SESSION["lingua"] == "ingles" ? "COMMENTS" : "DEIXE SEU COMENTÁRIO" ?></h2>	
								<div style="display:block;padding:20px">							
									<div id="disqus_thread"></div> <script>
									var disqus_config = function () { this.page.url = location.href; this.page.identifier = <?=$post[0]->id;?>  }; 
									(function() {  var d = document, s = d.createElement('script'); s.src = '//catscanada.disqus.com/embed.js'; s.setAttribute('data-timestamp', +new Date()); (d.head || d.body).appendChild(s); })();
									</script>
								</div>
							</div>
							
						</div>
						
						<? $this->load->view("_principal/sidebar.php"); ?>
						
					</div>
				</div>
				
			</div>
		</div>
	</section>
	