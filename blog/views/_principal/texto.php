	
	<section>
		<div id="page">
			<div class="container">
				<div class="row">
					<div class="col-md-8">
						<div class="texto">
							<? foreach ($texto as $row) { ?>
								<h1 class="entry-title"> <?=$_SESSION["lingua"] == "ingles" ? $row->title : $row->titulo ?> </h1>
								<img src="<?=base_url()?>upload/texto/<?=$row->imagem?>" alt="" />
								<p align="center"><b><i><?=$_SESSION["lingua"] == "ingles" ? $row->description : $row->descricao ?></i></b></p>
								<?=$_SESSION["lingua"] == "ingles" ? $row->text : $row->texto ?>
							<? } ?>
						</div>
					</div>
					
					<? $this->load->view("_principal/sidebar.php"); ?>
					
				</div>
			</div>
		</div>
	</section>
	