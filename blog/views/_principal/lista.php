<h2 id="resultadoBusca">Resultado da Busca</h2>

<? foreach ($posts as $row) { ?>
<article class="post postParaBusca" name="<?=$_SESSION["lingua"] == "ingles" ? $row->title : $row->titulo ?>">
	<a class="img" href="<?=base_url()?>post/<?=url($row->categoria)?>/<?=url($row->titulo)?>/<?=$row->id?>" data-post="<?=$row->titulo?>"><img src="<?=base_url()?>upload/post/<?=$row->imagem?>" alt="" class="principal" /></a>		
	<a class="entry-category" href="<?=base_url()?>categoria/<?=url($row->categoria)?>/<?=$row->idcategoria?>" data-post="<?=$row->titulo?>"><?=$_SESSION["lingua"] == "ingles" ? $row->category : $row->categoria ?></a>
	<h2 class="entry-title"><a href="<?=base_url()?>post/<?=url($row->categoria)?>/<?=url($row->titulo)?>/<?=$row->id?>" class="titulo-post" data-post="<?=$row->titulo?>"><?=$_SESSION["lingua"] == "ingles" ? $row->title : $row->titulo ?></a></h2>		
	<p class="entry-summary"><?=$_SESSION["lingua"] == "ingles" ? $row->summary : $row->resumo ?></p>
	<a class="entry-btn" href="<?=base_url()?>post/<?=url($row->categoria)?>/<?=url($row->titulo)?>/<?=$row->id?>" data-post="<?=$row->titulo?>"><?=$_SESSION["lingua"] == "ingles" ? "Continue reading" : "Continuar lendo" ?></a>
	<p class="entry-itens">
		<span class="entry-data"><img class="icon" src="<?=base_url()?>_imagens/icon-calendar.png" alt="" /> <?=formatarData($row->data)?></span>
		<span class="entry-share">
			<a href="http://www.facebook.com/sharer/sharer.php?u=<?=base_url()?>post/<?=url($row->categoria)?>/<?=url($row->titulo)?>/<?=$row->id?>" class="linkShare" data-social="Facebook" data-post="<?=$row->titulo?>"><img src="<?=base_url()?>_imagens/face.png" alt="" /></a>
			<a href="http://twitter.com/intent/tweet?text=<?=base_url()?>post/<?=url($row->categoria)?>/<?=url($row->titulo)?>/<?=$row->id?>" class="linkShare" data-social="Twitter" data-post="<?=$row->titulo?>"><img src="<?=base_url()?>_imagens/twitter.png" alt="" /></a>
			<a href="https://plus.google.com/share?url=<?=base_url()?>post/<?=url($row->categoria)?>/<?=url($row->titulo)?>/<?=$row->id?>" class="linkShare" data-social="GooglePlus" data-post="<?=$row->titulo?>"><img src="<?=base_url()?>_imagens/plus.png" alt="" /></a>
		</span>
	</p>
</article>
<? } ?>
