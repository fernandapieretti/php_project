<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta charset="utf-8">
	<meta name="robots" content="noindex,nofollow" />
	<title>Blog | Login</title>
	<link href="<?=base_url()?>css/bootstrap.min.css" rel="stylesheet" />
	<link href="<?=base_url()?>css/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet" />
	<link href="<?=base_url()?>css/sb-admin.css" rel="stylesheet" />
	<link href="<?=base_url()?>_imagens/favico.ico" rel="shortcut icon" />
</head>

<body>
	<div class="container">
		<div class="row">
			<div class="col-md-4 col-md-offset-4">
				<div class="login-panel panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">Informe seus dados</h3>
					</div>
					<div class="panel-body">
						<img src="<?=base_url()?>_imagens/logo.png" alt="" title="" style="display:block;margin:auto;max-width:100%" /> <br />
						<?= ($this->session->flashdata('erroUsuarioNaoLogado') != "" ) ? "<div class='alert alert-danger'>" . $this->session->flashdata('erroUsuarioNaoLogado') . "</div>" : "" ?>
						<?= $error != "" ? '<div class="alert alert-danger">' . $error . '</div>' : "" ?>
						<br>
						<form role="form" action="<?=base_url()?>login/login/autenticar" method="post">
							<fieldset>
								<div class="form-group">
									<input class="form-control" placeholder="Usuário" name="usuarioEmail" id="usuarioEmail" type="text" autofocus>
								</div>
								<div class="form-group">
									<input class="form-control" placeholder="Senha" name="senha" id="senha" type="password" value="">
								</div>
								<input type="submit" class="btn btn-lg btn-success btn-block" value="Entrar" />
							</fieldset>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>