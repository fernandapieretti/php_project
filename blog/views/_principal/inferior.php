	<footer>
		<div id="footer">				
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<p>Blog &copy; <?= date("Y") ?></p>
					</div>
				</div>
			</div>			
		</div>
	</footer>
	
	<script type="text/javascript" src="<?=base_url()?>_js/scripts.min.js"></script>

	</body>
</html>