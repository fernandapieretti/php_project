
	<section>
		<div id="erro">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<h1> 404 </h1>
						<p> <?=$_SESSION["lingua"] == "ingles" ? "Sorry, page cannot be found" : "Esta página não foi encontrada." ?> </p>
					</div>
				</div>
			</div>
		</div>
	</section>
	