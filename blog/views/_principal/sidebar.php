	<div class="col-md-3 col-md-offset-1">							
		<div id="sidebar" class="sidebar-fix">
		
			<? if ($posts) { ?>
			<input type="text" placeholder="<?=$_SESSION["lingua"] == "ingles" ? "Search by title..." : "Busca por título..." ?>" id="inputBusca" />
			<? } ?>
			
			<? if ($texto[0]->id <> 1) { ?>
			<div class="box profile">
				<h2><?=$_SESSION["lingua"] == "ingles" ? $sobre[0]->title : $sobre[0]->titulo ?></h2>
				<a href="<?=base_url()?>texto/<?=url($sobre[0]->titulo)?>/<?=$sobre[0]->id?>"><img src="<?=base_url()?>upload/texto/<?=$sobre[0]->imagem?>" alt="" /></a>
				<p><?=$_SESSION["lingua"] == "ingles" ? $sobre[0]->description : $sobre[0]->descricao ?> <br /> <br /> <a href="<?=base_url()?>texto/<?=url($sobre[0]->titulo)?>/<?=$sobre[0]->id?>" class="sobre-nos"><?=$_SESSION["lingua"] == "ingles" ? "Read more" : "Ler mais" ?></a></p>
			</div>
			<? } ?>
			
			<div class="box">
				<h2>Facebook</h2>
				<div class="fb-page" data-href="https://www.facebook.com/catscanada" data-width="260" data-small-header="true" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/catscanada"><a href="https://www.facebook.com/catscanada">catscanada</a></blockquote></div></div>
			</div>
			
			<div class="box">
				<h2>Instagram</h2>
				<script src="//lightwidget.com/widgets/lightwidget.js"></script><iframe src="//lightwidget.com/widgets/955b15516aa45281940d44d14a71d66c.html" id="lightwidget_955b15516a" name="lightwidget_955b15516a"  scrolling="no" allowtransparency="true" class="lightwidget-widget" style="width: 100%; border: 0; overflow: hidden;"></iframe>
			</div>
			
			<div class="box">
				<h2><?=$_SESSION["lingua"] == "ingles" ? "Adds" : "Publicidade" ?></h2>
				<p>BANNER</p>
			</div>
			
			<div class="box">
				<h2><?=$_SESSION["lingua"] == "ingles" ? "Most read" : "Posts mais lidos" ?></h2>
				<? $count = 0; foreach ($maislidos as $row) { $count = $count + 1; ?>
				<p class="mais-lidos">
					<a href="<?=base_url()?>post/<?=url($row->categoria)?>/<?=url($row->titulo)?>/<?=$row->id?>" class="link-mais-lido" data-post="<?=$row->titulo?>">
						<img src="<?=base_url()?>upload/post/<?=$row->imagem?>" alt="" />
						<?=$_SESSION["lingua"] == "ingles" ? $row->title : $row->titulo ?>
						<small>(<?=$row->visualizacao?>)</small>
					</a>
				</p>
				<? } ?>
			</div>		
			
		</div>		
	</div>