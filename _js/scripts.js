﻿/* Scripts catscanada.com.br */

//Saudação do leitor
$("#saudacaoLeitor").html($("#valorSaudacaoLeitor").val());
$("#periodoDia").html($("#valorPeriodoDia").val());

// Gerais
$(document).ready(function () {
	
	//Link Share nos Posts
	$('.linkShare').click(function (event) {
		var top = ($(window).height() / 2) - (345 / 2);
		var left = ($(window).width() / 2) - (570 / 2);
		window.open($(this).attr("href"), "popupWindow", "menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600,top=" + top + ", left=" + left);
		dataLayer.push({
			'event': 'linkShare',
			'socialMedia' : $(this).attr("data-social"),
			'post' : $(this).attr("data-post")
		});
		return false;
	});

	//Busca
    $("#inputBusca").keyup(function(){
		if ($(this).val() != "") {
			var resultado = 0;
			var count = 0;
			var valor = $(this).val().toUpperCase();
			
			$(".postParaBusca").css("display","none");
			$(".postParaBusca").each(function(){
				if($(this).attr("name").toUpperCase().indexOf(valor) >= 0){
					$(this).css("display","block");
					resultado = 1;
					count = count + 1;
				}
			});
			if (resultado == 1) {
				$("#resultadoBusca").css("display","block").html("Resultado da busca: " + count);
			} else {
				$("#resultadoBusca").css("display","block").html("Nenhum resultado encontrado :(");
			}
			dataLayer.push({
				'event': 'busca',
				'termo' : $(this).val()
			});
		} else {
			$("#resultadoBusca").css("display","none");
			$(".postParaBusca").css("display","block");
		}
    });
		
	// Side bar
    $('.sidebar-fix').affix({ offset: { top: 70, bottom: 265 } })

});  
